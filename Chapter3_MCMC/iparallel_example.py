#! /usr/bin/python



#  File:   iparallel_example.py
#  Author: @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
  
#  Created on May 8, 2015, 12:57:00 PM




__author__="Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de"
__date__ ="$May 8, 2015 12:57:00 PM$"

import sys
sys.path = ['/usr/local/lib/python2.7/dist-packages'] + sys.path # to fix the problem with numpy: this replaces  1.6 version by 1.9

import numpy as np
import pymc as pm


p = pm.Uniform("p", 0, 1)
assignment = pm.Categorical("assignment", [p, 1 - p], size=data.shape[0])
taus = 1.0 / pm.Uniform("stds", 0, 100, size=2) ** 2
centers = pm.Normal("centers", [120, 190], [0.01, 0.01], size=2)


@pm.deterministic
def center_i(assignment=assignment, centers=centers):
    return centers[assignment]

@pm.deterministic
def tau_i(assignment=assignment, taus=taus):
    return taus[assignment]

observations = pm.Normal("obs", center_i, tau_i, value=data, observed=True)

# below we create a model class
model = pm.Model([p, assignment, observations, taus, centers])

mcmc = pm.MCMC(model)
mcmc.sample(50000)
